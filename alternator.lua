local herbs = "découverte d'herbes"
local ore = "découverte de gisements"
local timer = 0
local updateTimeMax = 4
local started = false
local herbsActive = false
local oreActive = false


local Alternator = {};
Alternator.fight = false

local f = CreateFrame("Frame",nil,UIParent)
f:Show()

f:RegisterEvent("PLAYER_REGEN_ENABLED")
f:RegisterEvent("PLAYER_REGEN_DISABLED")
f:SetScript("OnUpdate", function(self, elapsed)
	timer = timer + elapsed
	if timer > updateTimeMax and started == true then
		update()
		timer = 0
		updateTimeMax = math.random(2,3)
	end
end)
f:SetScript("OnEvent", function(this, event, ...)
	if event=="PLAYER_REGEN_DISABLED" then
		Alternator.fight = true
	else
		Alternator.fight = false
	end
end)


function isSpellKnown( spell )
	local name = GetSpellInfo( spell )
	if name == nil then
		print('Spell not know ' .. spell)
		return false
	else
		print('Added spell: ' .. spell)
		return true
	end
end


function main()
	print('Alternator Started !')
	isSpellKnown(herbs)
	isSpellKnown(ore)
	CastSpellByName(herbs)
	started = true
end

function update()

	targetName = UnitName("target")
	if targetName ~= nil then
		return
	end

	if  Alternator.fight then
		return
	end
	
	
	
	if herbsActive == false then
		herbsActive = true
		CastSpellByName(herbs)
	else 
		herbsActive = false
		CastSpellByName(ore)
	end
	
end


main()


